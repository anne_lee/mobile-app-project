import { atom } from 'recoil'

export const postDB = atom({
  key: 'postDB',
  default: [
    // {
    //   title: "Fire detected at somewhere",
    //   location: "Barcelona",
    //   userName: "aaa",
    // }
  ]
})

export const commentDB = atom({
  key: 'commentDB',
  default: []
})

export const userDB = atom({
  key: 'userDB',
  default: ""
})