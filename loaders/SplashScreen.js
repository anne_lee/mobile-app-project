import React from 'react'
import { StyleSheet, Text, View, Button } from 'react-native'

const SplashScreen =()=> {

  return <View styles={styles.container}>
    <Text>Loading the app...</Text>
  </View>
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
})

export default SplashScreen