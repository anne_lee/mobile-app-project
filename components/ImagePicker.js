import React, { useState, useEffect, useRef } from 'react';
import { Button, Image, View, Platform, StyleSheet } from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import { Video } from 'expo-av';
import { NEXT_PUBLIC_CLOUDINARY_CLOUD_NAME, NEXT_PUBLIC_CLOUDINARY_UPLOAD_PRESET, url } from '../config';
import axios from 'axios'

export default function ImagePickerExample(props) {
  const videoRef = useRef({ src: "" });
  const [status, setStatus] = useState({});

  useEffect(() => {
    (async () => {
      if (Platform.OS !== 'web') {
        const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== 'granted') {
          alert('Sorry, we need camera roll permissions to make this work!');
        }
      }
    })();
  }, []);

  const handleImage = async (result, formData) => {
    formData.append("file", `data:${result.type}/${result.uri.slice(-3)};base64,${result.base64}`)
    formData.append("upload_preset", NEXT_PUBLIC_CLOUDINARY_UPLOAD_PRESET)
    try {
      const url = `https://api.cloudinary.com/v1_1/${NEXT_PUBLIC_CLOUDINARY_CLOUD_NAME}/upload`;
      const res = await axios.post(url, formData)
      props.setMedia({
        secure_url: res.data.secure_url,
        type: res.data.resource_type,
        public_id: res.data.public_id
      })
    } catch (err) {
      console.error(err)
    }
  }

  const handleVideo = async (result, formData) => {
    const randomWords = await axios.get("https://random-word-api.herokuapp.com/word?number=3")
    formData.append('video-upload', {
      name: `${randomWords.data[0]}_${randomWords.data[1]}_${randomWords.data[2]}`,
      type: 'video/mov',
      uri: result.uri
    })
    try {
      let response = await axios({
        method: 'post',
        url: url + '/media/upload_video',
        data: formData,
        headers: { 'Content-Type': 'multipart/form-data' }
      })
      props.setMedia(response.data.result)
      videoRef.current.src = response.data.result.secure_url
    }
    catch (err) {
      console.error(err)
    }
  }


  const pickMedia = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
      base64: true
    });
    if (result.cancelled === true) return false
    let formData = new FormData()
    if (result.type === 'image') {
      handleImage(result, formData)
    } else if (result.type === 'video') {
      handleVideo(result, formData)
    } else {
      return false
    }
  }

  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      {props.mediaObj.type === "image"
        ? <Image source={{ uri: props.mediaObj.secure_url }} style={{ width: 200, height: 200 }} />
        : props.mediaObj.type === "video"
          ? <View style={styles.container}>
            <Video
              ref={videoRef}
              style={styles.video}
              source={{ uri: props.mediaObj.secure_url }}
              useNativeControls
              resizeMode="contain"
              isLooping
              onPlaybackStatusUpdate={status => setStatus(() => status)}
            />
            <View style={styles.buttons}>
              <Button
                title={status.isPlaying ? 'Pause' : 'Play'}
                onPress={() =>
                  status.isPlaying ? videoRef.current.pauseAsync() : videoRef.current.playAsync()
                }
              />
            </View>
          </View>
          : props.mediaObj.type !== "image" && props.mediaObj.type !== "video"
          && <Button 
            title="Pick an image from camera roll" onPress={pickMedia} />}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
  },
  video: {
    alignSelf: 'center',
    width: 320,
    height: 200,
  },
  buttons: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
})


