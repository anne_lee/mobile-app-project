import React from 'react';
import { StyleSheet, View } from 'react-native'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import Constants from 'expo-constants'

const AutoComp = (props) => {
  return (
    <View style={styles.container}>
      <GooglePlacesAutocomplete
        placeholder='Search'
        onPress={(data, details = null) => {
          //props.setLocation()
        }}
        query={{
          key: 'AIzaSyAh84I0dQKgRyi1Pm3Dij12mflo5hmmyE8',
          language: 'en',
          types: 'geolocation'
        }}      
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
  }
})

export default AutoComp;