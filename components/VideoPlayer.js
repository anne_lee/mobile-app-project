import React, { useEffect, useRef, useState } from 'react'
import { Button, StyleSheet, View } from 'react-native';
import { Video } from 'expo-av';

export default function VideoPlayer(props) {

  const videoRef = useRef({ src: "" });
  const [status, setStatus] = useState({})

  useEffect(() => {
    videoRef.current.src = props.video.secure_url
  }, [])

  return <View>
    <Video
      ref={videoRef}
      style={styles.video}
      source={{ uri: props.video.secure_url }}
      useNativeControls
      resizeMode="contain"
      isLooping
      onPlaybackStatusUpdate={status => setStatus(() => status)}
    />
    <View style={styles.buttons}>
      <Button
        title={status.isPlaying ? 'Pause' : 'Play'}
        onPress={() =>
          status.isPlaying ? videoRef.current.pauseAsync() :
            videoRef.current.playAsync()
        }
      />
    </View>
  </View>
}

const styles = StyleSheet.create({
  video: {
    alignSelf: 'center',
    width: 300,
    height: 200,
  },
  buttons: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
})