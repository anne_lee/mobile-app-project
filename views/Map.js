import React from 'react'
import { View, Text, StyleSheet, Dimensions } from 'react-native'
import MapView, { Marker } from 'react-native-maps';

const Map = (props) => {

  if (props.userLocation.lat) {
    return <View style={{ flex: 1 }}>
      <MapView style={styles.map}
        initialRegion={{
          latitude: props.userLocation.lat,
          longitude: props.userLocation.lng,
          latitudeDelta: 0.1,
          longitudeDelta: 0.1
        }}
        provider="google"
      >
        {props.posts.map((post) => {
          return <Marker
            key={post._id}
            coordinate={{
              latitude: post.location.lat,
              longitude: post.location.lng
            }}
          />
        }
        )}
      </MapView>
    </View>
  } else {
    return <View styles={styles.container}>
      <Text>Loading Information...</Text>
    </View>
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  map: {
    flex: 1,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
  },
  locationmarker: {
    height: 300,
    width: 300,
  }
})

export default Map

