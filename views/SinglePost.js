import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, Image, Button, ScrollView, TextInput, TouchableOpacity, Alert } from 'react-native'
import Map from './Map'
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios'
import { url } from '../config'
import MapView from 'react-native-maps';
import moment from 'moment'
import { FontAwesome5 } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import VideoPlayer from '../components/VideoPlayer';

const SinglePost = (props) => {

  const [comment, setComment] = useState('')
  const [commentMessage, setCommentMessage] = useState('')
  const [deleteMessage, setDeleteMessage] = useState('')
  const [commentsForPost, setCommentsForPost] = useState([])
  const [isOwner, setIsOwner] = useState(false)

  const postTitle = props.postObj.title
  const owner = props.postObj.userId
  const navigation = useNavigation()

  const handleSubmitComment = async () => {
    if (!comment) return alert('Enter comment')
    try {
      const token = await AsyncStorage.getItem('token')
      axios.defaults.headers.common['Authorization'] = token
      const response = await axios.post(`${url}/comments/addcomment`, { comment, postId: props.postObj._id })
      if (response.data.ok) {
        setCommentMessage('Your comment has been added')
        setComment('')
        setTimeout(() => {
          setCommentMessage('')
        }, 4000)
        displayComments()
      } else {
        alert('Sign up to comment')
      }
    } catch (error) {
      console.error('error===>', error)
    }
  }

  const displayComments = async () => {
    try {
      const postId = props.postObj._id
      const response = await axios.get(`${url}/comments/findcomments/${postId}`)
      setCommentsForPost(response.data.comments)
    } catch (error) {
      console.error(error)
    }
  }

  useEffect(() => {
    displayComments()
    displayDeleteButton()
  }, [])

  const displayDeleteButton = async () => {
    try {
      const token = await AsyncStorage.getItem('token')
      axios.defaults.headers.common['Authorization'] = token
      const response = await axios.post(`${url}/users/verify_token`)
      if (response.data.succ._id === owner) {
        setIsOwner(true)
      }
    } catch (error) {
      console.error('error>>>', error)
    }
  }

  const handleDelete = async () => {
    try {
      const token = await AsyncStorage.getItem('token')
      axios.defaults.headers.common['Authoriztion'] = token
      const response = await axios.post(`${url}/posts/deletepost`, { postId: props.postObj._id })
      if (response.data.ok) {
        setDeleteMessage('Post Deleted')
        setTimeout(() => {
          setDeleteMessage('')
          if (props.goBackTo === 'home') {
            props.setScreen('home')
          } else if (props.goBackTo === 'profile')
            navigation.navigate('Home')
        }, 4000)
      } else {
        alert('try again')
      }
    } catch (error) {
      console.error(error)
    }
  }

  const deleteAlert = () => {
    Alert.alert(
      "Do you want to delete this post?",
      postTitle,
      [
        {
          text: "Cancel",
          style: "cancel"
        },
        {
          text: "OK",
          onPress: () => handleDelete()
        }
      ],
      { cancelable: false }
    );
  }

  return <View style={{ flex: 1, backgroundColor: '#DBE6FD' }}>
    <View style={{ flex: 0.07, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', height: 50, backgroundColor: '#47597E' }}>
      {props.goBackTo === 'home'
        ? <Button title="＜" onPress={() => props.setScreen("home")} />
        : <Button title="＜" onPress={() => props.setProfileScreen(true)} />
      }
      <Text style={{ fontSize: 20, color: '#DBE6FD' }}>{props.postObj.title}</Text>

      {isOwner
        ? deleteMessage === 'Post Deleted'
          ? <Text style={{ color: 'red' }}>{deleteMessage}</Text>
          : <TouchableOpacity style={{ marginRight: 8 }} onPress={() => deleteAlert()}><FontAwesome5 name="trash-alt" size={24} color="black" /></TouchableOpacity>
        : <Text> </Text>
      }
    </View>

    <ScrollView style={{ flex: 0.9 }}>
      <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginVertical: 15 }}>
        <MapView style={{ height: 120, width: 150, marginRight: 15 }} >
          <Map
            userLocation={props.postObj.location}
            posts={[props.postObj]}
          />
        </MapView>

        <View >
          <Text>{moment(props.postObj.createdAt).format('LL')}</Text>
          <Text>{moment(props.postObj.createdAt).format('LT')}</Text>
          <Text>{moment(props.postObj.createdAt).format('dddd')}</Text>

          <Text style={{ marginTop: 10 }}>Post by: {props.postObj.userName}</Text>
        </View>
      </View>

      <View style={{ alignItems: 'center' }}>
        <View style={{ marginVertical: 10 }}>
          {props.postObj.image.map(ele =>
            <Image style={styles.image}
              source={{
                uri: ele.secure_url
              }}
              key={ele.id}
            />
          )}
        </View>

        <View style={{ marginVertical: 5 }}>
          {props.postObj.video.map(ele =>
            <VideoPlayer
              video={ele}
              key={ele.id}
            />
          )}
        </View>
      </View>

      {commentsForPost.map(comment =>
        <View key={comment._id} style={{ marginVertical: 10, alignItems: 'flex-start' }}>
          <Text>
            <Text style={{ fontSize: 10 }}>{comment.userId.username}：</Text>
            <Text style={{ fontSize: 20 }}>{comment.comment}</Text>
          </Text>
          <Text style={{ fontSize: 10 }}>{moment(comment.createdAt).format('lll')}</Text>
        </View>
      )}

      <Text style={{ color: 'red' }}>{commentMessage}</Text>
      <View style={{ marginTop: 10 }}>
        <View style={{ flexDirection: 'row', width: '90%', justifyContent: 'center', alignItems: 'center', }}>
          <TextInput
            onChangeText={(text) => setComment(text)}
            value={comment}
            style={{ borderWidth: 1, width: '80%', borderRadius: 5, height: 35, backgroundColor: 'white' }} />
          <TouchableOpacity onPress={() => handleSubmitComment()}>
            <Text
              style={{ justifyContent: 'center', fontSize: 12, marginLeft: 10 }}>comment</Text>
          </TouchableOpacity>
        </View>
      </View>

    </ScrollView >
  </View>
}

const styles = StyleSheet.create({
  image: {
    height: 200,
    width: 300,
    marginBottom: 5
  }
})

export default SinglePost
