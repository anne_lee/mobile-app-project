import React, { useState, useEffect } from 'react'
import { StyleSheet, View, Button, ScrollView, Text, Image, TouchableOpacity } from 'react-native'
import axios from 'axios'
import { url } from '../config'
import AsyncStorage from '@react-native-async-storage/async-storage';
import moment from 'moment'
import { Video } from 'expo-av'
import Map from './Map';
import MapView from 'react-native-maps';

const Profile = (props) => {

  const [userPosts, setUserPosts] = useState([])
  const [userName, setUserName] = useState('')

  const getUserName = async () => {
    try {
      const token = await AsyncStorage.getItem('token')
      axios.defaults.headers.common['Authorization'] = token
      const response = await axios.post(`${url}/users/verify_token`)
      if (response.data.ok) {
        setUserName(response.data.succ.username)
      }
    } catch (error) {
      console.error(error)
    }
  }

  const getUserPosts = async () => {
    try {
      const token = await AsyncStorage.getItem('token')
      axios.defaults.headers.common['Authorization'] = token
      const response = await axios.get(`${url}/posts/finduser`)
      setUserPosts(response.data.foundUser)
    } catch (error) {
      console.error(error)
    }
  }

  useEffect(() => {
    getUserName()
    getUserPosts()
  }, [props])

  const handleLogout = () => {
    props.logout(false)
  }

  const renderPosts = () => {
    return userPosts?.slice(0).reverse().map(post => {
      return <TouchableOpacity
        key={post._id}
        style={styles.eachpost}
        onPress={() => {
          props.setPostObj(post)
          props.setProfileScreen()
        }}>

        <View>
          {post.image.length > 0 &&
            <Image
              source={{
                uri: post.image[0].secure_url
              }}
              style={styles.image}
            />}

          {post.image.length === 0 && post.video.length > 0 &&
            <Video
              source={{
                uri: post.video[0].secure_url
              }}
              style={styles.image}
            />}

          {post.image.length === 0 && post.video.length === 0 &&
            <MapView style={styles.image}>
              <Map
                userLocation={post.location}
                posts={[post]}
              />
            </MapView>
          }
        </View>

        <View>
          <Text style={{ fontSize: 20, color: '#293B5F' }}>{post.title}</Text>
          <Text style={{ fontSize: 10, color: '#293B5F' }}>{moment(post.createdAt).format("MMM Do YY")}</Text>

        </View>
      </TouchableOpacity>
    })
  }


  return <View style={{ flex: 1, backgroundColor: '#DBE6FD' }}>
    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', backgroundColor: '#47597E' }}>
      <Text style={{ fontSize: 50, marginLeft: 15, color: '#DBE6FD' }}>{userName && userName}'s posts</Text>
      <Text style={{ justifyContent: 'center', color: '#DBE6FD' }}>Total: {userPosts ? userPosts.length : 0} posts</Text>
    </View>

    <ScrollView>
      <View>
        {renderPosts()}
      </View>

      <Button title="Logout" onPress={handleLogout} />
    </ScrollView>
  </View>
}


const styles = StyleSheet.create({
  eachpost: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 15,
    paddingHorizontal: '10%'
  },
  image: {
    width: 120,
    height: 90,
    alignItems: 'center',
    marginRight: 10,
  },
})

export default Profile