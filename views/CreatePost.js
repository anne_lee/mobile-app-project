import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, TextInput, Button, ScrollView } from 'react-native'
import { url } from '../config'
import MediaPicker from '../components/ImagePicker'
import axios from 'axios'
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { v4 as uuid } from 'uuid';

const CreatePost = (props) => {

  const [title, setTitle] = useState("")
  const [media, setMedia] = useState([])
  const [message, setMessage] = useState("")
  const navigation = useNavigation()

  const tempMedia = () => {
    let tempMedia = []
    for (let i = 0; i < 5; i++) {
      tempMedia.push({ id: uuid() })
    }
    setMedia(tempMedia)
  }

  useEffect(() => {
    tempMedia()
  }, [])



  const handleUpload = (item, i, id) => {
    let tempMedia = [...media]
    tempMedia[i] = { ...item, id }
    setMedia(tempMedia)
  }

  const handleSubmit = async () => {
    if (!title) return alert('Title is required')
    if (props.userLocation.lat === 0 && props.userLocation.lng === 0) return alert('Location is required')
    try {
      const token = await AsyncStorage.getItem('token')
      axios.defaults.headers.common['Authorization'] = token
      const response = await axios.post(`${url}/posts/createpost`, { title, location: props.userLocation, image: media.filter(ele => ele.type === 'image'), video: media.filter(ele => ele.type === 'video') })
      if (response.data.ok) {
        setMessage('Post successfully created')
        props.update()
        setTimeout(
          () => {
            setMessage('')
            setTitle('')
            tempMedia()
            navigation.navigate('Home')
          }, 4000)
      } else {
        alert('try again')
      }
    } catch (error) {
      console.error(error)
    }
  }

  return <View style={{ flex: 1, justifyContent: 'center', backgroundColor: '#DBE6FD' }}>

    <View style={{ flex: 0.2, justifyContent: 'center' }}>
      <Text style={{ marginLeft: 15, color: '#293B5F' }}>Title</Text>
      <TextInput
        value={title}
        style={{ width: '90%', height: 30, borderWidth: 1, borderRadius: 5, marginLeft: '5%', backgroundColor:'white' }}
        onChangeText={(text) => setTitle(text)}
      />
    </View>

    <View style={{ flex: 0.8 }}>
      <ScrollView>
        {media.map((ele, i) => {
          return <View key={ele.id} style={{ marginVertical: 10, marginLeft: 15 }}>
            <Text style={{ color: '#293B5F' }}>Image/Video {i + 1}</Text>
            <MediaPicker mediaObj={media[i]} setMedia={(media) => handleUpload(media, i, ele.id)} />
          </View>
        })}
      </ScrollView>
    </View>


    <View style={{ flex: 0.1 }}>
      <Text style={{ color: 'red' }}>{message}</Text>
      <Button onPress={handleSubmit} title="Create a Post" />
    </View>

  </View>
}



export default CreatePost