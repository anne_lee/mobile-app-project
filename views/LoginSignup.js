import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, TextInput, Button } from 'react-native'
import { url } from '../config'
import axios from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage';

const LoginSignup = (props) => {
  const [page, setPage] = useState("initial")
  const [form, setValues] = useState({
    username: '',
    email: '',
    password: '',
    password2: ''
  })
  const [message, setMessage] = useState("")

  const handleSignup = async () => {
    try {
      const response = await axios.post(`${url}/users/register`, {
        username: form.username,
        email: form.email,
        password: form.password,
        password2: form.password2
      })
      setMessage(response.data.message)
      if (response.data.ok) {
        // storeData(response.data.token)
        setTimeout(() => {
          setMessage('')
          props.setLoggedIn(false)
          setPage('login')
        }, 4000)
      }
    } catch (error) {
      console.error(error)
    }
  }

  const handleLogin = async () => {
    try {
      const response = await axios.post(`${url}/users/login`, {
        email: form.email,
        password: form.password
      })
      setMessage(response.data.message)
      if (response.data.ok) {
        storeData(response.data.token)
        setTimeout(() => {
          setMessage('')
          props.setLoggedIn(true)
        }, 3000)
      }
    } catch (error) {
      console.error(error)
    }
  }

  const storeData = async (token) => {
    try {
      await AsyncStorage.setItem('token', token)
    } catch (error) {
      console.error(error)
    }
  }

  const handleClear = () => {
    setValues('')
    setMessage('')
  }

  return <View style={{width: '100%',flex: 1,  backgroundColor: '#DBE6FD'}}>

    <View style={{ flex: 1, width: '100%', justifyContent: 'center', alignItems: 'center' }}>
      <Text style={{ fontSize: 70, color: '#293B5F' }}>iReport</Text>
    </View>

    {page === "initial"
      ? <View style={{ flex: 2, alignItems: 'center' }}>
        <Text style={{color: '#293B5F'}}>If you do not have an account</Text>
        <Button title="sign up" onPress={() => setPage("signup")} />

        <Text style={{ marginTop: 30, color: '#293B5F' }}>If you have an account</Text>
        <Button title="log in" onPress={() => setPage("login")} />
      </View>
      : page === "signup"
        ? <View style={{ flex: 3, alignItems: 'center' }}>
          <Text style={{ fontSize: 25, color: '#293B5F' }}>Sign Up</Text>
          <Text style={{ color: 'red' }}>{message}</Text>

          <Text style={{color: '#293B5F'}}>Username</Text>
          <TextInput
            style={styles.input}
            value={form.username}
            onChangeText={(text) => setValues({ ...form, username: text })}
          />

          <Text style={{color: '#293B5F'}}>Email</Text>
          <TextInput
            style={styles.input}
            value={form.email}
            onChangeText={(text) => setValues({ ...form, email: text })}
          />

          <Text style={{color: '#293B5F'}}>Password</Text>
          <TextInput style={styles.input}
            value={form.password}
            onChangeText={(text) => setValues({ ...form, password: text })}
          />

          <Text style={{color: '#293B5F'}}>Confirm Password</Text>
          <TextInput style={styles.input}
            value={form.password2}
            onChangeText={(text) => setValues({ ...form, password2: text })}
          />

          <Button title="Sign Up" onPress={handleSignup} />

          <Text style={{ marginTop: 15 }}>If you have an account</Text>
          <Button title="Log In" onPress={() => (
            setPage("login"),
            handleClear()
          )} />

        </View>
        : page === "login"
        && <View style={{ flex: 2, alignItems: 'center' }}>
          <Text style={{ fontSize: 25 }}>Log In</Text>
          <Text style={{ color: 'red' }}>{message}</Text>

          <Text style={{color: '#293B5F'}}>Email</Text>
          <TextInput style={styles.input}
            value={form.email}
            onChangeText={(text) => setValues({ ...form, email: text })}
          />

          <Text style={{color: '#293B5F'}}>Password</Text>
          <TextInput style={styles.input}
            value={form.password}
            onChangeText={(text) => setValues({ ...form, password: text })}
          />

          <Button title="Log In" onPress={handleLogin} />

          <Text style={{ marginTop: 20 }}>If you have an account</Text>
          <Button title="Sign Up" onPress={() => (
            setPage("signup"),
            handleClear()
          )} />
        </View>
    }
  </View>
}

//  <TouchableWithoutFeedback onPress={Keyboard.dismiss}/>
//   <LoginSignup />
// </TouchableWithoutFeedback>

const styles = StyleSheet.create({
  input: {
    height: 40,
    width: '80%',
    borderColor: '#293B5F',
    borderWidth: 1,
    marginBottom: 15,
    backgroundColor: 'white',
  }
})

export default LoginSignup