import React, { useEffect, useState } from 'react'
import { View, Text, StyleSheet, TextInput, Image, ScrollView, TouchableOpacity } from 'react-native'
import moment from 'moment'
import { Video } from 'expo-av'
import Map from './Map'
import MapView from 'react-native-maps'


const Home = (props) => {

  const [search, setSearch] = useState('')

  useEffect(() => {
    props.fetchPostsFromDB()
  }, [props])

  //↓↓↓↓↓↓↓↓Each Post↓↓↓↓↓↓↓↓↓↓↓↓

  const eachpost = props.posts?.slice(0).reverse().filter(ele => {
    if (ele.title.toLowerCase().includes(search.toLowerCase())) return true
    else { return false }
  }).map((ele) => {
    return <TouchableOpacity key={ele._id} style={styles.eachpost} onPress={() => {
      props.setPostObj(ele)
      props.setScreen()
    }
    }  >
      <View style={{ flex: 1 }}>
        {ele.image.length > 0 &&
          <Image
            source={{
              uri: ele.image[0].secure_url
            }}
            style={styles.image}
          />}

        {ele.image.length === 0 && ele.video.length > 0 &&
          <Video
            source={{
              uri: ele.video[0].secure_url
            }}
            style={styles.image}
          />}

        {ele.image.length === 0 && ele.video.length === 0 &&
          <MapView style={styles.image}>
            <Map
              userLocation={ele.location}
              posts={[ele]}
            />
          </MapView>}
      </View>

      <View style={{flex:1, alignItems: 'flex-start'}}>
        <Text style={{fontSize: 20, color:'#293B5F'}}> {ele.title} </Text>
        <Text style={{fontSize: 10, color:'#293B5F'}}> {moment(ele.createdAt).format("LLL")}</Text>
      </View>
    </TouchableOpacity>
  })

  //ーーーーーーーーーRenderingーーーーーーーーーーー

  return (
    <View style={{ flex: 1, backgroundColor: '#DBE6FD' }}>

      <View style={styles.container}>
        <Map userLocation={props.userLocation} posts={props.posts} />
      </View>

      <View style={{ flex: 1 }}>
        <TextInput
          style={styles.search}
          placeholder={"  Search"}
          onChangeText={(text) => setSearch(text)}
        />


        <ScrollView>
          {props.posts.length === 0
            ? <View>
              <Text>There are no posts yet</Text>
            </View>
            : <View>
              {eachpost}
            </View>}
        </ScrollView>


      </View>

    </View>
  )
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  search: {
    height: 40,
    width: "100%",
    borderWidth: 1,
    borderRadius: 10,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  timeline: {
    flex: 1,
  },
  eachpost: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
    paddingHorizontal: '10%',
  },
  image: {
    width: 120,
    height: 90,
    alignItems: 'center',
  },
})

export default Home