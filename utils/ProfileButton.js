import React from 'react'
import { Ionicons } from "@expo/vector-icons";
import { useNavigation } from '@react-navigation/native';

export default function ProfileButton(props) {
  const navigation = useNavigation()
  return (
    <Ionicons
      name="person-circle-outline"
      size={30}
      color="black"
      onPress={() => {
        props.setProfileScreen()
        navigation.navigate('Profile')
      }}
    />
  )
}