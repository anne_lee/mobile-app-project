import React from 'react'
import { AntDesign } from "@expo/vector-icons";
import { useNavigation } from '@react-navigation/native';

export default function HomeButton(props) {
  const navigation = useNavigation()
  return (
    <AntDesign
      name="home" size={30}
      color="black"
      onPress={() =>{
        props.setScreen()
        navigation.navigate('Home')
      }}
    />
  )
}