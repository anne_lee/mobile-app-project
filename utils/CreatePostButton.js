import React from 'react'
import { AntDesign } from "@expo/vector-icons";
import { useNavigation } from '@react-navigation/native';

export default function CreatePostButton(props) {
  const navigation = useNavigation()
  return (
    <AntDesign
      name="plussquareo"
      size={30} color="black"
      onPress={() =>
        navigation.navigate('Create Post')
      }
    />
  )
}