import React, { useState, useEffect, useCallback } from 'react';
import { View, Platform, SafeAreaView, StyleSheet } from "react-native";
import { StatusBar } from 'expo-status-bar'
import * as SplashScreen from "expo-splash-screen"
import AsyncStorage from '@react-native-async-storage/async-storage';
import LoadingScreen from './loaders/SplashScreen'
import LoginSignup from './views/LoginSignup';
import Router from "./Router";
import { RecoilRoot } from 'recoil'
import Constants from "expo-constants";

export default function App(props) {
  const [isAndroid, setIsAndroid] = useState(null)
  const [appIsReady, setAppIsReady] = useState(false)
  const [loggedIn, setLoggedIn] = useState(false)

  useEffect(() => {
    const getOperatingSystem = () => {
      Platform.OS === 'ios' ? setIsAndroid(false) : setIsAndroid(true)
    }
    getOperatingSystem();
    const prepare = async () => {
      try {
        await SplashScreen.preventAutoHideAsync()
        getData()
      } catch (error) {
        console.error(error)
      } finally {
        setAppIsReady(true)
      }
    }
    prepare();
  }, [])

  const getData = async () => {
    const token = await AsyncStorage.getItem('token')
    if (token !== null) {
      setLoggedIn(true)
    }
  }

  const hideSplash = useCallback(async () => {
    if (appIsReady) {
      await SplashScreen.hideAsync()
    }
  }, [appIsReady])


  if (!appIsReady) {
    return <LoadingScreen />
  } else if (!loggedIn) {
    return <View style={styles.container}
      onLayout={hideSplash}
    >
      <LoginSignup setLoggedIn={(bool) => setLoggedIn(bool)} />
    </View>
  } else if (appIsReady && loggedIn) {
    return <View style={{ flex: 1, backgroundColor: '#47597E' }} onLayout={hideSplash}>
      <StatusBar />
      {isAndroid
        ? <View style={{ marginTop: Constants.statusBarHeight, fontFamily: 'Roboto' }}>
          <Router />
        </View>
        : <SafeAreaView style={{ flex: 1 }}>
          <RecoilRoot>
            <Router 
            logout={(bool) => {
              AsyncStorage.removeItem("token")
              setLoggedIn(bool)
            }} />
          </RecoilRoot>
        </SafeAreaView>
      }
    </View>
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  router: {
    flex: 1,
  },
  
})


