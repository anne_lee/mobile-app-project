import React, { useState, useEffect } from 'react'
import Home from './views/Home'
import CreatePost from './views/CreatePost';
import Profile from './views/Profile'
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { NavigationContainer } from "@react-navigation/native";
import SinglePost from './views/SinglePost';
import * as Location from 'expo-location'
import { postDB } from './state'
import { useRecoilValue } from 'recoil'
import { useSetRecoilState } from 'recoil'
import axios from 'axios';
import { url } from './config'
import ProfileButton from './utils/ProfileButton';
import CreatePostButton from './utils/CreatePostButton';
import HomeButton from './utils/HomeButton';

const Router = (props) => {
  const Stack = createBottomTabNavigator()
  const Tab = createBottomTabNavigator()

  const [postObj, setPostObj] = useState({
    _id: '',
    title: '',
    location: '',
    createdAt: '',
    image: [],
    video: [],
  })
  const [screen, setScreen] = useState("home")
  const [profileScreen, setProfileScreen] = useState(true)
  const [userLocation, setUserLocation] = React.useState({
    lat: 0,
    lng: 0
  })
  const [errorMsg, setErrorMsg] = React.useState(null)

  /////////////////////////////////////////////////////////

  const setPostDBState = useSetRecoilState(postDB)
  const posts = useRecoilValue(postDB)

  const fetchPostsFromDB = async () => {
    try {
      const allPosts = await axios.get(`${url}/posts/allposts`)
      setPostDBState(allPosts.data.allPosts)
    } catch (error) {
      console.error(error)
    }
  }

  useEffect(() => {
    posts.length < 1 && fetchPostsFromDB()
  }, [])


  //==============Getting Location===============

  React.useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync()
      if (status !== 'granted') {
        setErrorMsg('Permission to access location was denied')
        return
      }
      let userLocation = await Location.getCurrentPositionAsync({})
      setUserLocation({
        lat: userLocation.coords.latitude,
        lng: userLocation.coords.longitude
      })
    })()
  }, [])

  // //instead of setting text, alert it
  // let text = 'Waiting...'
  // if (errorMsg) {
  //   text = errorMsg
  // } else if (userLocation) {
  //   text = JSON.stringify(userLocation)
  // }

  return (
    <NavigationContainer>
      <Stack.Navigator tabBarOptions={{
            style: { backgroundColor: '#47597E' }
          }}>

        <Tab.Screen
          name="Home"
          style={{ padding: 5, }}
          options={{
            tabBarLabel: "Home",
            tabBarIcon: ({ focused, color, size }) => (
              <HomeButton setScreen={() => setScreen('home')} />
            )
          }}
          
          children={() => {
            if (screen === "home") {
              return <Home
                setPostObj={(data) => setPostObj(data)}
                setScreen={() => setScreen("")}
                userLocation={userLocation}
                posts={posts}
                fetchPostsFromDB={fetchPostsFromDB}
              />
            } else {
              return <SinglePost
                postObj={postObj}
                setScreen={() => setScreen("home")}
                goBackTo={'home'} />
            }
          }
          }
        />

        <Tab.Screen
          name="Create Post"
          style={{ padding: 5 }}
          options={{
            tabBarLabel: "Create Post",
            tabBarIcon: ({ focused, color, size }) => (
              <CreatePostButton />
            )
          }}
          children={() => {
            return <CreatePost {...props} update={fetchPostsFromDB} userLocation={userLocation} />
          }}
        />


        <Tab.Screen
          name="Profile"
          style={{ padding: 5 }}
          options={{
            tabBarLabel: "Profile",
            tabBarIcon: ({ focused, color, size }) => (
              <ProfileButton setProfileScreen={() => setProfileScreen(true)} />
            )
          }}
          children={() => {
            if (profileScreen === true) {
              return <Profile
                setPostObj={(data) => setPostObj(data)}
                setProfileScreen={() => (setProfileScreen(false))}
                logout={props.logout}
                posts={posts}
              />
            } else {
              return <SinglePost
                postObj={postObj}
                setProfileScreen={() => setProfileScreen(true)}
                goBackTo={'profile'} />
            }
          }}
        />


      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default Router